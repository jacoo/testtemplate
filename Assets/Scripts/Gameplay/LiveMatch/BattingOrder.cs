﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public static class BattingOrderHelper 
{
	public static void Initialise(ref Dictionary<int, string> batting, int inningsNumber)
	{
		batting.Clear();

		switch(inningsNumber)
		{
			case 1:
				// Openers
				batting.Add (1, "Batter1");
				batting.Add (2, "Batter2");
				// Top Order
				batting.Add (3, "Batter3");
				batting.Add (4, "Batter4");
				// Middle Order
				batting.Add (5, "Batter5");
				batting.Add (6, "Batter6");
				batting.Add (7, "Batter7");
				// Low Order
				batting.Add (8, "Batter8");
				batting.Add (9, "Batter9");
				batting.Add (10, "Batter10");
				batting.Add (11, "Batter11");
			break;
			case 2:
				// Openers
				batting.Add (1, "Batter12");
				batting.Add (2, "Batter13");
				// Top Order
				batting.Add (3, "Batter14");
				batting.Add (4, "Batter15");
				// Middle Order
				batting.Add (5, "Batter16");
				batting.Add (6, "Batter17");
				batting.Add (7, "Batter18");
				// Low Order
				batting.Add (8, "Batter19");
				batting.Add (9, "Batter20");
				batting.Add (10, "Batter21");
				batting.Add (11, "Batter22");
			break;
		}
	}
}