﻿using UnityEngine;
using System.Collections;

public class SlideManager : MonoBehaviour 
{
	/// <summary>
	/// Enables the bool animator.
	/// </summary>
	/// <param name="anim">Animation.</param>
	public void EnableBoolAnimator(Animator anim)
	{
		if (anim.GetBool ("IsDisplayed") == true) 
		{
			anim.SetBool ("IsDisplayed", false);
		} 
		else 
		{
			anim.SetBool ("IsDisplayed", true);
		}
	}
}