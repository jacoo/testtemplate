﻿using UnityEngine;

public class Team : ScriptableObject 
{
	public string Name { get; set; }

	public string League { get; set; }

}