﻿public struct Over 
{
	public int Number { get; set; }
	public int Ballnumber { get; set; }

	public Bowler Bowler { get; set; }

	public int Runs { get; set; }
}