﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System;

public class LiveCommentary : MonoBehaviour 
{
	#region Fields
	public Team team1;
	public Team team2;

	int team1runs = 0;
	int team2runs = 0;

	MatchType matchType;

	Inning inning;
	Over over;

	string Batter1 = string.Empty;
	string Batter2 = string.Empty;

	string CurrentBowler = string.Empty;
	int CurrentBowlerIndex = 1;

	int ballCountForNextOver = 7;

	bool BallLive = false;
	bool AllOut = false;

	int runs = 0;
	int batter1runs = 0;
	int batter2runs = 0;

	int nextBatter = 3;
	int receivingBatter = 1;
	bool changeBatter = false;
	bool dismissed = false;
	
	Dictionary<int, int> RunChance = new Dictionary<int, int>();
	Dictionary<int, string> BattingOrder = new Dictionary<int, string>();
	Dictionary<int, string> Batters = new Dictionary<int, string>();
	Dictionary<int, string> Bowlers = new Dictionary<int, string>();

	Dictionary<Penalty, int> Penalties = new Dictionary<Penalty, int>();
	Dictionary<Dismissal, int> Dismissals = new Dictionary<Dismissal, int>();
	Dictionary<int, Pause> Pauses = new Dictionary<int, Pause>();

	GameObject gameObjectContent;
	Text textContent;
	#endregion Fields

	/// <summary>
	/// Start this instance.
	/// </summary>
	void Start () 
	{
		matchType = MatchType.Twenty20;
		gameObjectContent = GameObject.FindGameObjectWithTag("Content");
		textContent = gameObjectContent.GetComponent<Text>();
		inning.Number = 1;
		over.Number = 1;
		over.Ballnumber = 1;
		textContent.text = "";
		Populate();
		Batter1 = BattingOrder[1];
		Batter2 = BattingOrder[2];
	}

	/// <summary>
	/// Update this instance.
	/// </summary>
	void Update () 
	{
		if(!BallLive && !AllOut)
		{
			StartCoroutine(GetNextBall());
		}
	}

	/// <summary>
	/// Populate this instance.
	/// </summary>
	void Populate()
	{
		BattingOrderHelper.Initialise(ref BattingOrder, inning.Number);

		RunChance.Add(0, 72); // 72% chance
		RunChance.Add(1, 20); // 20% chance
		RunChance.Add(2, 20); // 20% chance
		RunChance.Add(3, 20); // 20% chance
		RunChance.Add(4,  4); //  4% chance
		RunChance.Add(5,  4); //  4% chance
		RunChance.Add(6, 10); // 10% chance

		// Each dismissal has a 10% chance
		Dismissals.Add(Dismissal.Retired, 10);
		Dismissals.Add(Dismissal.Bowled, 10);
		Dismissals.Add(Dismissal.TimedOut, 10);
		Dismissals.Add(Dismissal.Caught, 10);
		Dismissals.Add(Dismissal.HandledTheBall, 10);
		Dismissals.Add(Dismissal.HitTheBallTwice, 10);
		Dismissals.Add(Dismissal.HitWicket, 10);
		Dismissals.Add(Dismissal.LegBeforeWicket, 10);
		Dismissals.Add(Dismissal.ObstructingTheField, 10);
		Dismissals.Add(Dismissal.RunOut, 10);
		Dismissals.Add(Dismissal.Stumped, 10);

		// Each penalty has a 10% chance
		Penalties.Add(Penalty.NoBall, 10);
		Penalties.Add(Penalty.Wide, 10);

		Bowlers.Add (1, "Bowler nr1");
		Bowlers.Add (2, "Bowler nr2");
		Bowlers.Add (3, "Bowler nr3");
		Bowlers.Add (4, "Bowler nr4");
		Bowlers.Add (5, "Bowler nr5");
		Bowlers.Add (6, "Bowler nr6");
		Bowlers.Add (7, "Bowler nr7");
		Bowlers.Add (8, "Bowler nr8");
		Bowlers.Add (9, "Bowler nr9");
		Bowlers.Add (10, "Bowler nr10");
		Bowlers.Add (11, "Bowler nr11");

		Batters.Add (1, "Batter1");
		Batters.Add (2, "Batter2");
		Batters.Add (3, "Batter3");
		Batters.Add (4, "Batter4");
		Batters.Add (5, "Batter5");
		Batters.Add (6, "Batter6");
		Batters.Add (7, "Batter7");
		Batters.Add (8, "Batter8");
		Batters.Add (9, "Batter9");
		Batters.Add (10, "Batter10");
		Batters.Add (11, "Batter11");

		// T20 only has a Drinks pause
		Pauses.Add (1, Pause.Luncheon);
		Pauses.Add (2, Pause.Drinks);
		Pauses.Add (3, Pause.Tea);
	}
	
	/// <summary>
	/// Processes the ball outcome.
	/// </summary>
	/// <returns>The ball outcome.</returns>
	private string ProcessBallOutcome()
	{
		int outcomeChooser = GetRandomnessForBallOutcome();
		string outcome = string.Empty;

		switch(outcomeChooser)
		{
			case 0: // NO RUNS
				{
					outcome = "no run";
					break;	
				}
			case 1: // 1 RUN
				{
					outcome = "1 run";
					changeBatter = true;
					AddRunsToUI(1);
					break;	
				}
			case 2: // 2 RUNS
				{
					outcome = "2 runs";
					AddRunsToUI(2);
					break;	
				}
			case 3: // 3 RUNS
				{
					outcome = "3 runs";
					changeBatter = true;
					AddRunsToUI(3);
					break;	
				}
			case 4: // FOUR
				{
					outcome = "FOUR";
					AddRunsToUI(4);
					break;	
				}
			case 5: // SIX
				{
					outcome = "SIX";
					AddRunsToUI(6);
					break;	
				}
			case 6: // OUT
				{
					string reason = WeightedRandomizer.From(Dismissals).TakeOne().ToString();
					outcome = "OUT: " + reason;
					string reasonshort = GetReasonshort(reason);
	
					if(receivingBatter == 1)
					{
						SetBatterUIText(Batter1, reasonshort);
					}
					else
					{
						SetBatterUIText(Batter2, reasonshort);
					}
	
					dismissed = true;
					break;	
				}
		}

		return outcome;
	}
		
	/// <summary>
	/// Updates when the ball is dead.
	/// </summary>
	private void BallDeadUpdates()
	{
		over.Ballnumber++;

		if (dismissed)
		{
			if(nextBatter == 12)
			{
				Debug.Log("ALL OUT !!!!!!");
				textContent.text += "\n" + "ALL OUT !!!!!!\n\n";

				if (inning.Number == 1) 
				{
					team1runs = runs;
				} 
				else 
				{
					team2runs = runs;
				}

				ResetForNextInning();
			}
			else
			{
				if (receivingBatter == 1)
				{
					Batter1 = BattingOrder[nextBatter];
					nextBatter++;
					batter1runs = 0;
				}
				else
				{
					Batter2 = BattingOrder[nextBatter];
					nextBatter++;
					batter2runs = 0;
				}
			}	

			dismissed = false;
		}

		if(changeBatter)
		{
			if (receivingBatter == 1)
			{
				receivingBatter = 2;
			}
			else
			{
				receivingBatter = 1;
			}

			changeBatter = false;
		}

		if(over.Ballnumber == ballCountForNextOver)
		{
			if (over.Runs == 0)
			{
				Debug.Log("**** END OF OVER " + over.Number + " (MAIDEN) ****");
				textContent.text += "\n" + "**** END OF OVER " + over.Number + " (MAIDEN) ****\n";
			}
			else
			{
				Debug.Log("**** END OF OVER " + over.Number + " ****");
				textContent.text += "\n" + "**** END OF OVER " + over.Number + " ****\n";
			}

			ResetForNextOver();
		}

		if (over.Number == 21) 
		{
			if (inning.Number == 1) 
			{
				team1runs = runs;
			} 
			else 
			{
				team2runs = runs;
			}

			ResetForNextInning();
		}
			
		CheckForEndOfMatch();

		BallLive = false;
		GameObject gameObject2 = GameObject.FindGameObjectWithTag("ScrollView");
		ScrollRect scrollRect = gameObject2.GetComponent<ScrollRect>();
		Canvas.ForceUpdateCanvases();
		scrollRect.verticalScrollbar.value=0f;
	}

	/// <summary>
	/// Checks for end of match.
	/// </summary>
	private void CheckForEndOfMatch()
	{
		if (inning.Number == 3) 
		{
			if (team1runs > team2runs) 
			{
				Debug.Log("**** END OF MATCH **** team 1 runs = " + team1runs + ", team 2 runs = " + team2runs + " team 1 wins");
				textContent.text += "\n" + "**** END OF MATCH **** team 1 runs = " + team1runs + ", team 2 runs = " + team2runs + " team 1 wins\n";
			} 
			else 
			{
				Debug.Log("**** END OF MATCH **** team 1 runs = " + team1runs + ", team 2 runs = " + team2runs + " team 2 wins");
				textContent.text += "\n" + "**** END OF MATCH **** team 1 runs = " + team1runs + ", team 2 runs = " + team2runs + " team 2 wins\n";

			}

			AllOut = true;
		}
	}

	/// <summary>
	/// Resets for next over.
	/// </summary>
	private void ResetForNextOver()
	{
		over.Runs = 0;
		over.Ballnumber = 1;
		over.Number++;
		CurrentBowlerIndex++;

		if (CurrentBowlerIndex == 12) 
		{
			CurrentBowlerIndex = 1;
		}
	}

	/// <summary>
	/// Resets for next inning.
	/// </summary>
	private void ResetForNextInning()
	{
		Batters.Clear();

		Batters.Add (1,  "Batter12");
		Batters.Add (2,  "Batter13");
		Batters.Add (3,  "Batter14");
		Batters.Add (4,  "Batter15");
		Batters.Add (5,  "Batter16");
		Batters.Add (6,  "Batter17");
		Batters.Add (7,  "Batter18");
		Batters.Add (8,  "Batter19");
		Batters.Add (9,  "Batter20");
		Batters.Add (10, "Batter21");
		Batters.Add (11, "Batter22");

		over.Runs = 0;
		over.Ballnumber = 1;
		over.Number = 1;

		CurrentBowlerIndex = 1;
		receivingBatter = 1;

		inning.Number++;

		if (inning.Number == 2) 
		{
			BattingOrderHelper.Initialise (ref BattingOrder, inning.Number);

			runs = 0;
			batter1runs = 0;
			batter2runs = 0;
			nextBatter = 3;
			Batter1 = BattingOrder [1];
			Batter2 = BattingOrder [2];
		}
	}

	/// <summary>
	/// Gets the next ball.
	/// </summary>
	/// <returns>The next ball.</returns>
	IEnumerator GetNextBall() 
	{
		BallLive = true;
		CurrentBowler = Bowlers[CurrentBowlerIndex];
		string outcome = ProcessBallOutcome();

		if (receivingBatter == 1)
		{
			Debug.Log(over.Number + "." + over.Ballnumber + "  " + CurrentBowler + " to " + Batter1 + ", " + outcome + ", " + runs + " runs");
			textContent.text += "\n" + over.Number + "." + over.Ballnumber + "  " + CurrentBowler + " to " + Batter1 + ", " + outcome + ", " + runs + " runs";
		}
		else
		{
			Debug.Log(over.Number + "." + over.Ballnumber + "  " + CurrentBowler + " to " + Batter2 + ", " + outcome + ", " + runs + " runs");
			textContent.text += "\n" + over.Number + "." + over.Ballnumber + "  " + CurrentBowler + " to " + Batter2 + ", " + outcome + ", " + runs + " runs";
		}

		yield return new WaitForSeconds(0.01f);
		BallDeadUpdates();
	}

	/// <summary>
	/// Gets the randomness for ball outcome.
	/// </summary>
	/// <returns>The randomness for ball outcome.</returns>
	int GetRandomnessForBallOutcome()
	{
		int outcome = WeightedRandomizer.From(RunChance).TakeOne();
		
		return outcome;
	}

	/// <summary>
	/// Gets the short reason for dismissal.
	/// </summary>
	/// <returns>The reasonshort.</returns>
	/// <param name="reason">Reason.</param>
	private string GetReasonshort(string reason)
	{
		string reasonshort = string.Empty;

		switch(reason)
		{
			case "Retired":
				reasonshort = "RET";
				break;
			case "Bowled":
				reasonshort = "B";
				break;
			case "TimedOut":
				reasonshort = "TO";
				break;
			case "Caught":
				reasonshort = "C";
				break;
			case "HandledTheBall":
				reasonshort = "HTB";
				break;
			case "HitTheBallTwice":
				reasonshort = "HTBT";
				break;
			case "HitWicket":
				reasonshort = "HW";
				break;
			case "LegBeforeWicket":
				reasonshort = "LBW";
				break;
			case "ObstructingTheField":
				reasonshort = "OTF";
				break;
			case "RunOut":
				reasonshort = "RO";
				break;
			case "Stumped":
				reasonshort = "S";
				break;
		}

		return reasonshort;
	}

	#region UI
	/// <summary>
	/// Adds the runs to UI.
	/// </summary>
	/// <param name="newruns">Newruns.</param>
	private void AddRunsToUI(int newruns)
	{
		string tag = string.Empty;
		over.Runs += newruns;
		runs += newruns;

		if (receivingBatter == 1)
		{
			GameObject gameObject = GameObject.FindGameObjectWithTag(Batter1);
			Text text = gameObject.GetComponent<Text>();
			batter1runs += newruns;
			text.text = batter1runs.ToString();
		}
		else
		{
			GameObject gameObject = GameObject.FindGameObjectWithTag(Batter2);
			Text text = gameObject.GetComponent<Text>();	
			batter2runs += newruns;
			text.text = batter2runs.ToString();
		}
	}
	/// <summary>
	/// Sets the batter user interface text.
	/// </summary>
	/// <param name="Batter">Batter.</param>
	/// <param name="reasonshort">Reasonshort.</param>
	private void SetBatterUIText(string Batter, string reasonshort)
	{
		GameObject gameObject;

		switch(Batter)
		{
			case "Batter1":
			case "Batter12":
				if (inning.Number == 1) 
				{
					gameObject = GameObject.FindGameObjectWithTag ("Out1");
				} 
				else 
				{
					gameObject = GameObject.FindGameObjectWithTag ("Out12");
				}

				Text text = gameObject.GetComponent<Text>();
				text.text = reasonshort;
				break;
			case "Batter2":
			case "Batter13":
				if (inning.Number == 1) 
				{
					gameObject = GameObject.FindGameObjectWithTag ("Out2");
				} 
				else 
				{
					gameObject = GameObject.FindGameObjectWithTag ("Out13");
				}

				text = gameObject.GetComponent<Text>();
				text.text = reasonshort;
				break;
			case "Batter3":
			case "Batter14":
				if (inning.Number == 1) 
				{
					gameObject = GameObject.FindGameObjectWithTag ("Out3");
				} 
				else 
				{
					gameObject = GameObject.FindGameObjectWithTag ("Out14");
				}

				text = gameObject.GetComponent<Text>();
				text.text = reasonshort;
				break;
			case "Batter4":
			case "Batter15":
				if (inning.Number == 1) 
				{
					gameObject = GameObject.FindGameObjectWithTag ("Out4");
				} 
				else 
				{
					gameObject = GameObject.FindGameObjectWithTag ("Out15");
				}

				text = gameObject.GetComponent<Text>();
				text.text = reasonshort;
				break;
			case "Batter5":
			case "Batter16":
				if (inning.Number == 1) 
				{
					gameObject = GameObject.FindGameObjectWithTag ("Out5");
				} 
				else 
				{
					gameObject = GameObject.FindGameObjectWithTag ("Out16");
				}

				text = gameObject.GetComponent<Text>();
				text.text = reasonshort;
				break;
			case "Batter6":
			case "Batter17":
				if (inning.Number == 1) 
				{
					gameObject = GameObject.FindGameObjectWithTag ("Out6");
				} 
				else 
				{
					gameObject = GameObject.FindGameObjectWithTag ("Out17");
				}
				
				text = gameObject.GetComponent<Text>();
				text.text = reasonshort;
				break;
			case "Batter7":
			case "Batter18":
				if (inning.Number == 1) 
				{
					gameObject = GameObject.FindGameObjectWithTag ("Out7");
				} 
				else 
				{
					gameObject = GameObject.FindGameObjectWithTag ("Out18");
				}
				
				text = gameObject.GetComponent<Text>();
				text.text = reasonshort;
				break;
			case "Batter8":
			case "Batter19":
				if (inning.Number == 1) 
				{
					gameObject = GameObject.FindGameObjectWithTag ("Out8");
				} 
				else 
				{
					gameObject = GameObject.FindGameObjectWithTag ("Out19");
				}
				
				text = gameObject.GetComponent<Text>();
				text.text = reasonshort;
				break;
			case "Batter9":
			case "Batter20":
				if (inning.Number == 1) 
				{
					gameObject = GameObject.FindGameObjectWithTag ("Out9");
				} 
				else 
				{
					gameObject = GameObject.FindGameObjectWithTag ("Out20");
				}

				text = gameObject.GetComponent<Text>();
				text.text = reasonshort;
				break;
			case "Batter10":
			case "Batter21":
				if (inning.Number == 1) 
				{
					gameObject = GameObject.FindGameObjectWithTag ("Out10");
				} 
				else 
				{
					gameObject = GameObject.FindGameObjectWithTag ("Out21");
				}

				text = gameObject.GetComponent<Text>();
				text.text = reasonshort;
				break;
			case "Batter11":
			case "Batter22":
				if (inning.Number == 1) 
				{
					gameObject = GameObject.FindGameObjectWithTag ("Out11");
				} 
				else 
				{
					gameObject = GameObject.FindGameObjectWithTag ("Out22");
				}

				text = gameObject.GetComponent<Text>();
				text.text = reasonshort;
				break;
		}
	}
	#endregion UI
}