﻿public enum Dismissal 
{
	Retired,
	Bowled,
	TimedOut,
	Caught,
	HandledTheBall,
	HitTheBallTwice,
	HitWicket,
	LegBeforeWicket,
	ObstructingTheField,
	RunOut,
	Stumped
}