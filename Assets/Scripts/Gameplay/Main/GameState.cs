﻿using UnityEngine;

public class GameState : ScriptableObject 
{
	public int Money { get; set; }
}