﻿using System;
using UnityEngine;
using BestHTTP;
using BestHTTP.WebSocket;
using BestHTTP.Examples;
using System.Threading;
//using Serializers;

public class NetworkManager : MonoBehaviour
{
    #region Private Fields
    /// <summary>
    /// The WebSocket address to connect
    /// </summary>
	//string address = "ws://localhost:3251/PublicGatewayWS";
	public string address = "ws://stadiagaming.northeurope.cloudapp.azure.com:3251/PublicGatewayWS/";

    private bool Sent = false;

    /// <summary>
    /// Default text to send
    /// </summary>
//	string msgToSend = "Hello World!";
	
    /// <summary>
    /// Debug text to draw on the gui
    /// </summary>
	string Text = string.Empty;
	
    /// <summary>
    /// Saved WebSocket instance
    /// </summary>
	WebSocket webSocket;
	
    /// <summary>
    /// GUI scroll position
    /// </summary>
	//Vector2 scrollPos;
    #endregion
	
    #region Unity Events
	void OnDestroy()
	{
		if (webSocket != null)
			webSocket.Close();
	}
	
    void Start()
    {
        webSocket = new WebSocket(new Uri(address));

//#if !BESTHTTP_DISABLE_PROXY && !UNITY_WEBGL
//        if (HTTPManager.Proxy != null)
//            webSocket.InternalRequest.Proxy = new HTTPProxy(HTTPManager.Proxy.Address, HTTPManager.Proxy.Credentials, false);
//#endif

        webSocket.OnOpen += OnOpen;
        webSocket.OnMessage += OnMessageReceived;
        webSocket.OnClosed += OnClosed;
        webSocket.OnError += OnError;

        webSocket.Open();

        // webSocket.Close(1000, "Bye!");
    }
    #endregion

    void Update()
    {
        if (webSocket != null && webSocket.IsOpen && !Sent)
        {
            webSocket.Send("test");
            Sent = true;
        }
    }

    #region WebSocket Event Handlers

    /// <summary>
    /// Called when the web socket is open, and we are ready to send and receive data
    /// </summary>
    void OnOpen(WebSocket ws)
    {
	    //Text += string.Format("-WebSocket Open!\n");
	    Debug.Log("WebSocket Open!");
    }

    /// <summary>
    /// Called when we received a text message from the server
    /// </summary>
    void OnMessageReceived(WebSocket ws, string message)
	{
		Debug.Log("Message received from SF: " + message);
        //Text += string.Format("-Message received: {0}\n", message);
    }

    /// <summary>
    /// Called when the web socket closed
    /// </summary>
    void OnClosed(WebSocket ws, UInt16 code, string message)
    {
	    //Text += string.Format("-WebSocket closed! Code: {0} Message: {1}\n", code, message);
	    Debug.Log("WebSocket closed!");
        webSocket = null;
    }

    /// <summary>
    /// Called when an error occured on client side
    /// </summary>
    void OnError(WebSocket ws, Exception ex)
    {
        string errorMsg = string.Empty;
#if !UNITY_WEBGL || UNITY_EDITOR
        if (ws.InternalRequest.Response != null)
            errorMsg = string.Format("Status Code from Server: {0} and Message: {1}", ws.InternalRequest.Response.StatusCode, ws.InternalRequest.Response.Message);
#endif

        Text += string.Format("-An error occured: {0}\n", (ex != null ? ex.Message : "Unknown Error " + errorMsg));
	    Debug.Log("WebSocket Error! " + errorMsg);
        webSocket = null;
    }

    #endregion
}