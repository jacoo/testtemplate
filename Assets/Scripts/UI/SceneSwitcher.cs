﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class SceneSwitcher : MonoBehaviour
{
	public void SwitchScene(string scene)
	{
		SceneManager.LoadScene(scene);
    }
}