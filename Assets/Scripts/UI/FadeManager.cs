﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class FadeManager : MonoBehaviour {

    public float FadeTime = 2f;
    public float DelayTime = 5f;

    public int NextSceneIndex = 2;

    public Image faderImage;

    private float m_delayTimer;

	// Use this for initialization
	void Start () 
	{       
		// StartCoroutine(FadeIn());
	}
	
	// Update is called once per frame
	void Update () {

        if (m_delayTimer > 0)
        {
            m_delayTimer = Mathf.Clamp(m_delayTimer - Time.deltaTime, 0f, float.MaxValue);

            if (m_delayTimer == 0)
            {
                StartCoroutine(FadeOut());
            }
        }

	}

    public IEnumerator FadeIn()
    {
        faderImage.CrossFadeAlpha(0f, FadeTime, true);
        yield return new WaitForSeconds(FadeTime);
        m_delayTimer = DelayTime;
    }

    public IEnumerator FadeOut()
    {
        faderImage.CrossFadeAlpha(1f, FadeTime, true);
        yield return new WaitForSeconds(FadeTime);
        UnityEngine.SceneManagement.SceneManager.LoadScene(NextSceneIndex);
    }
}
