﻿using System;
using UnityEngine;

namespace AssemblyCSharp
{
	public class NameGenerator : MonoBehaviour
	{
		void Start()
		{
			for(int x = 0; x < 10; x++)
			{
				string firstName = generateName(UnityEngine.Random.Range(4, 7));
				string familyName = generateName(UnityEngine.Random.Range(6, 9));
				Debug.Log(firstName + " " + familyName);
			}
		}

		public static string generateName(int length)
		{
			UnityEngine.Random r = new UnityEngine.Random();
			string[] consonants = {"b","c","d","f","g","h","j","k","l","m","n","p","q","r","s","sh","z","zh",
				"t","v","w","x","y"};
			string[] vowels = { "a", "e", "i", "o", "u"};
			string stringName; //Name as a string
			string[] arrayName = new string[length]; //Name as array
			bool Cons; //Whether the next letter added to the name array is a consonant or not.
			int a = UnityEngine.Random.Range(0, 2); //Used to determine the value of the boolean Cons
			if (a == 1) Cons = true; else Cons = false;
			Cons = true;
			int i = 1;
			Cons = false;
			while (i < length)
			{
				if(Cons)
				{
					arrayName[i] = consonants[UnityEngine.Random.Range(0, consonants.Length)];
					Cons = false;
					i++;
				}
				else
				{
					arrayName[i] = vowels[UnityEngine.Random.Range(0, vowels.Length)];
					Cons = true;
					i++;
				}
			}
			arrayName[1] = arrayName[1].ToUpper();
			stringName = string.Join("", arrayName); 
			return stringName;
		}
	}
}