﻿using System;
using UnityEngine;
using BestHTTP;
using BestHTTP.WebSocket;
using BestHTTP.Examples;

public class WSClient : MonoBehaviour
{
    #region Private Fields

    /// <summary>
    /// The WebSocket address to connect
    /// </summary>
    //string address = "wss://echo.websocket.org";

    /// <summary>
    /// Default text to send
    /// </summary>
//    string msgToSend = "Hello World!";

    /// <summary>
    /// Debug text to draw on the gui
    /// </summary>
    string Text = string.Empty;

    /// <summary>
    /// Saved WebSocket instance
    /// </summary>
    //WebSocket webSocket;

    /// <summary>
    /// GUI scroll position
    /// </summary>
    Vector2 scrollPos;

    #endregion

    #region Unity Events

    //void OnDestroy()
    //{
    //    if (webSocket != null)
    //        webSocket.Close();
    //}


    #endregion

    #region WebSocket Event Handlers

    /// <summary>
    /// Called when the web socket is open, and we are ready to send and receive data
    /// </summary>
    void OnOpen(WebSocket ws)
    {
        Text += string.Format("-WebSocket Open!\n");
    }

    /// <summary>
    /// Called when we received a text message from the server
    /// </summary>
    void OnMessageReceived(WebSocket ws, string message)
    {
        Text += string.Format("-Message received: {0}\n", message);
    }

    /// <summary>
    /// Called when the web socket closed
    /// </summary>
    void OnClosed(WebSocket ws, UInt16 code, string message)
    {
        Text += string.Format("-WebSocket closed! Code: {0} Message: {1}\n", code, message);
        //webSocket = null;
    }

    /// <summary>
    /// Called when an error occured on client side
    /// </summary>
    void OnError(WebSocket ws, Exception ex)
    {
        string errorMsg = string.Empty;
#if !UNITY_WEBGL || UNITY_EDITOR
        if (ws.InternalRequest.Response != null)
            errorMsg = string.Format("Status Code from Server: {0} and Message: {1}", ws.InternalRequest.Response.StatusCode, ws.InternalRequest.Response.Message);
#endif

        Text += string.Format("-An error occured: {0}\n", (ex != null ? ex.Message : "Unknown Error " + errorMsg));

        //webSocket = null;
    }

#endregion
}