﻿using System;
using UnityEngine;

namespace StadiaGaming.Helpers
{
    #region Event Handler Delegates
	public delegate void PlayerEnabledEventHandler(object sender, PlayerEnabledEventArgs e);
    #endregion

	public static partial class EventSink
    {
        #region Event Handlers
	    public static event PlayerEnabledEventHandler playerEnabled;
        #endregion
	    
	    public static void Reset()
	    {
			playerEnabled = null;
	    }
	    
        #region Invoke Methods
	    public static void InvokePlayerEnabled(object sender, PlayerEnabledEventArgs e)
	    {
			if (playerEnabled != null) playerEnabled(sender, e);
	    }
        #endregion
    }

    #region Event Arg Classes
    public class PlayerEnabledEventArgs : EventArgs
    {
	    private int m_player;
	    
	    public int Player { get { return m_player; } }
	    
		public PlayerEnabledEventArgs(int player)
	    {
		    m_player = player;
	    }
    }
    #endregion
}